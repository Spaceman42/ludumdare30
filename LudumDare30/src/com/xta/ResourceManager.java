package com.xta;

import java.awt.Image;
import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.Map;

public class ResourceManager {
	public static final String ASSET_PATH = "assets/";
	
	private static ResourceManager instance;
	
	private Map<String, Image> images;
	
	private ResourceManager() {
		images = new HashMap<String, Image>();
	}
	
	
	public void loadImageDirectory(String directory) {
		File folder = new File(ASSET_PATH + directory);
		File[] imageList = folder.listFiles(new FileFilter() {
			String[] extensions = {".jpg", ".png", ".gif"};
			@Override
			public boolean accept(File file) {
				for (String s : extensions) {
					if (file.isFile() && file.getName().toLowerCase().endsWith(s)) {
						return true;
					}
				}
				return false;
			}
		});
		
		for (File image : imageList) {
			System.out.println(image.getName());
			images.put(image.getName(), Utils.loadImage(image));
		}
	}
	
	public Image getImage(String name) {
		return images.get(name);
	}
	
	
	public static ResourceManager getInstance() {
		if (instance == null) {
			instance = new ResourceManager();
		}
		return instance;
	}
}
