package com.xta;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

import com.xta.game.Game;
import com.xta.graphics.DrawPanel;

public class Main {
	
	
	
	public static void main(String[] args) {
		Main main = new Main();
		main.run();
	}
	
	private DrawPanel drawPanel;
	private JFrame frame;
	private Game game;
	
	private float deltaTime;
	
	public void run() {
		game = new Game();
		
		createFrame();
		
		deltaTime = getNanos();
		
		Timer updateTimer = new Timer(32, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				deltaTime = Math.abs(getNanos()- deltaTime);
				
				game.onUpdate(deltaTime);
				drawPanel.repaint();
				
				deltaTime = getNanos();
			}
		});
		updateTimer.start();
	}
	
	private float getNanos() {
		return (float)(System.nanoTime()/1000000.0D);
	}
	
	private void createFrame() {
		JFrame frame = new JFrame();
		
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		drawPanel = new DrawPanel(game);
		drawPanel.setPreferredSize(new Dimension(600, 600));
		frame.getContentPane().add(drawPanel);
		frame.pack();
		frame.setVisible(true);
	}
}
