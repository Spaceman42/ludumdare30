package com.xta.game.cell;

import java.awt.Graphics2D;

import com.xta.game.Cell;
import com.xta.game.Grid;
import com.xta.math.Vector2i;

public class EmptyCell extends Cell{

	public EmptyCell(Grid grid) {
		super(grid, 0, 0, 0, new Vector2i());
	}

	@Override
	public CellType getType() {
		return CellType.EMPTY;
	}

	@Override
	protected void onUpdate(float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onDraw(Graphics2D g2) {
		// TODO Auto-generated method stub
		
	}

}
