package com.xta.game.cell;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import com.xta.RenderManager;
import com.xta.game.Cell;
import com.xta.game.Grid;
import com.xta.game.Planet;
import com.xta.math.Vector2f;
import com.xta.math.Vector2i;

public class AirCell extends Cell {

	private float particles = 100f;
	
	private float particleMax = 1000f;

	private List<Vector2f> forces;

	public AirCell(Grid grid, int gridX, int gridY, float sizeinworld,
			Vector2i planetCenter) {
		super(grid, gridX, gridY, sizeinworld, planetCenter);
		setZ(1000f);
	}

	@Override
	public CellType getType() {
		return CellType.AIR;
	}

	@Override
	protected void onUpdate(float deltaTime) {
		applyFoces();

		forces = new ArrayList<Vector2f>();

		// gravity.
		Vector2f gravity = new Vector2f(planetCenter)
				.sub(new Vector2f(getGridPosition()));
		gravity.normalize().mul(30);
		forces.add(gravity);
		
		Vector2f up = new Vector2f(0,-1).mul(particles/200.f);
		Vector2f right = new Vector2f(1,0).mul(particles/200.f);
		Vector2f down = new Vector2f(0,1).mul(particles/200.f);
		Vector2f left = new Vector2f(-1,0).mul(particles/200.f);
		
		forces.add(up);
		forces.add(right);
		forces.add(down);
		forces.add(left);
	}

	private void applyFoces() {
		if (forces != null) {
			for (Vector2f force : forces) {
				List<Cell> cells = getGrid().getAlongVector(getGridPosition(),
						force);

				for (Cell c : cells) {
					if (c == this) {
						continue;
					}
					if (c != null && c.getType() == CellType.AIR) {
						AirCell other = (AirCell) c;
						float dist = distanceTo(other);
						//if (dist < 5) {
							float power = inverseSquare(dist) * 5;
							if (other.particles + power < particleMax && particles-power >= 0) {
								particles -= power;
								other.particles += power;
							}
						//}
					}
				}
			}
		}

	}
	private float distanceTo(Cell other) {
		return getGrid().distanceTo(getGridPosition(), other.getGridPosition());
	}

	private float inverseSquare(float dist) {
		return 1.0f / (dist * dist);
	}

	@Override
	protected void onDraw(Graphics2D g2) {
		float adjusted = (float) ((particles+4400) / 4400)-4;
		//System.out.println(particles);
		int drawParts = (int)particles;
		if (drawParts < 0) {
			drawParts = 1;
		}
		int red=(255*(int)drawParts)/1000;
		int green=(255*(1000-(int)drawParts))/1000; 
		int blue=0;
		
		if (red < 0) {
			red = 0;
		}
		if (green < 0) {
			green = 0;
		}
		
		if (particles >= 980) {
			red = 255;
			green = 255;
			blue = 255;
		}
		if (particles < 20) {
			red = 0;
			green = 0;
			blue = 0;
		}
		
		Color color = new Color(red, green, blue);

		RenderManager.getInstance().drawRectangle(g2, color, getPosition(),
				getSizeInWorld(), getSizeInWorld());

	}

	public double getNumberOfParticles() {
		return particles;
	}

}
