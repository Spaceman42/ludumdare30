package com.xta.game.cell;

import java.awt.Color;
import java.awt.Graphics2D;



import com.xta.RenderManager;
import com.xta.game.Cell;
import com.xta.game.Grid;
import com.xta.math.Vector2i;

public class GroundCell extends Cell{

	


	

	public GroundCell(Grid grid, int gridX, int gridY, float sizeinworld,
			Vector2i planetCenter) {
		super(grid, gridX, gridY, sizeinworld, planetCenter);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onUpdate(float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onDraw(Graphics2D g2) {
		//System.exit(0);
		RenderManager.getInstance().drawRectangle(g2, Color.BLUE, getPosition(), getSizeInWorld(), getSizeInWorld());
	}

	@Override
	public CellType getType() {
		return CellType.GROUND;
	}

}
