package com.xta.game.cell;

public enum CellType {
	AIR, WATER, GROUND, EMPTY;
}
