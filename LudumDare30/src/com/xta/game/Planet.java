package com.xta.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import com.xta.RenderManager;
import com.xta.Utils;
import com.xta.entity.Entity;
import com.xta.math.GameMath;
import com.xta.math.Vector2f;
import com.xta.math.Vector2i;

public class Planet extends Entity {
	
	public static final int LOC_X = 50;
	public static final int LOC_Y = 50;
	
	public final int radius;
	
	public Planet(int radius) {
		setX(0f);
		setY(0f);
		setZ(10f);
		this.radius = 40;
	}
	
//	public List<Vector2i> getCells() {
//		List<Vector2i> points = new ArrayList<Vector2i>();
//		
//		int f = 1 - radius;
//		int ddFx = 1;
//		int ddFy = -2 * radius;
//		int x = 0;
//		int y = radius;
//		
//		points.add(new Vector2i(GRID_X, GRID_Y + radius));
//		points.add(new Vector2i(GRID_X, GRID_Y - radius));
//		points.add(new Vector2i(GRID_X + radius, GRID_Y));
//		points.add(new Vector2i(GRID_X - radius, GRID_Y));
//		
//		while(x<y) {
//			if (f >= 0) {
//				y--;
//				ddFy += 2;
//				f += ddFy;
//			}
//			x++;
//			ddFx += 2;
//			f += ddFx;
//			
//			points.add(new Vector2i(GRID_X + x, GRID_Y + y));
//			points.add(new Vector2i(GRID_X - x, GRID_Y + y));
//			points.add(new Vector2i(GRID_X + x, GRID_Y - y));
//			points.add(new Vector2i(GRID_X - x, GRID_Y - y));
//			points.add(new Vector2i(GRID_X + y, GRID_Y + x));
//			points.add(new Vector2i(GRID_X - y, GRID_Y + x));
//			points.add(new Vector2i(GRID_X + y, GRID_Y - x));
//			points.add(new Vector2i(GRID_X - y, GRID_Y - x));
//		}
//		
//		return points;
//	}
	
	public List<Vector2i> getCells() {
		return rasterizeWorld();
	}
	
	private Vector2f[] generateWorld(float radius, int numPoints, float deepest, float highest) {
		Vector2f[] points = new Vector2f[numPoints];
		for (int i = 0; i < points.length; i++) {
			float angle = ((float) i / numPoints) * GameMath.TWO_PI;
			points[i] = new Vector2f(GameMath.cos(angle) * radius, GameMath.sin(angle) * radius);
		}
		return points;
	}
	
	private List<Vector2i> rasterizeWorld() {
		Vector2f[] points = generateWorld(radius, 40, 0, 0);
		List<Vector2i> cells = new ArrayList<Vector2i>();
		for (int i = 0; i < points.length; i++) {
			if (i != points.length - 1) {
				cells.addAll(Utils.rasterizeLine(points[i], points[i + 1]));
			} else {
				cells.addAll(Utils.rasterizeLine(points[i], points[0]));
			}
		}
		for (Vector2i cell : cells)
			cell.add(new Vector2i(LOC_X, LOC_Y));
		return cells;
	}
	
	@Override
	protected void onUpdate(float deltaTime) {
	}
	
	@Override
	protected void onDraw(Graphics2D g2) {
		Color color = new Color(114, 83, 68);
		//RenderManager.getInstance().drawCircle(g2, getPosition(), radius-10, new Color(150, 75, 0));
	}
}
