package com.xta.game;

import java.util.ArrayList;
import java.util.List;

import com.xta.Utils;
import com.xta.game.cell.AirCell;
import com.xta.game.cell.CellType;
import com.xta.game.cell.GroundCell;
import com.xta.math.Vector2f;
import com.xta.math.Vector2i;

public class Grid {
	private static final float X = 0;
	private static final float Y = 0;
	private static final float SCENE_SIZE = 600;
	
	private Cell[][] cells;
	
	private final int height;
	private final int width;
	
	private float cellSize;
	private int numberOfCells;
	
	public Grid(int width, int height, Planet planet) {
		this.height = 100;
		this.width = 100;
		numberOfCells = height*width;
		cellSize = (float)SCENE_SIZE/(float)this.width;
		System.out.println("Cell size: " + cellSize);
		
		createCells(planet);
	}
	
	public Cell[] getCellsAround(Cell cell) {
		Cell[] results = new Cell[8];
		int x = cell.getGridX();
		int y = cell.getGridY();
		if (x > 0) {
			results[0] = cells[x-1][y];
			if (y > 0) {
				results[1] = cells[x-1][y-1];
				results[2] = cells[x][y-1];
			}
			if (y < height - 1) {
				results[7] = cells[x-1][y+1];
				results[6] = cells[x][y+1];
			}
		}
		if (x < width-1) {
			results[4] = cells[x+1][y];
			if (y > 0) {
				results[3] = cells[x+1][y-1];
			}
			if (y< height -1) {
				results[5] = cells[x+1][y+1];
			}
		}
		return results;
	}
	
	private void createCells(Planet planet) {
		cells = new Cell[width][height];
		
		List<Vector2i> planetCells = planet.getCells();
		System.out.println("Number of ground cells: " + planetCells.size());
		for (Vector2i point : planetCells) {

			//System.out.println("creating new ground cell at: " + point);
			cells[point.x][point.y] = new GroundCell(this, point.x, point.y, cellSize, new Vector2i(Planet.LOC_X, Planet.LOC_Y));
			System.out.println("creating new ground cell at: " + point);
		}
		long l = 0;
		long ground = 0;
		for (int x = 0; x<cells.length; x++) {
			for (int y = 0; y<cells[x].length; y++) {
				if (cells[x][y] == null && distanceTo(new Vector2i(x, y), new Vector2i(Planet.LOC_X, Planet.LOC_Y))>= planet.radius) {
					
					cells[x][y] = new AirCell(this, x, y, cellSize, new Vector2i(Planet.LOC_X, Planet.LOC_Y));
				}
				if (cells[x][y] != null) {
					cells[x][y].setPosition(getWorldPosition(x, y));
					l++;
				}
				if (cells[x][y] != null && cells[x][y].getType() == CellType.GROUND) {
					ground++;
				}
				
			}
		}
		System.out.println("Number of cells: " + l +". Number of Ground Cells: " + ground);
		System.out.println("Optimization was " + ((double)l/(numberOfCells))*100 +"% effective");
	}
	
	public double getAllAirParts() {
		double parts = 0;
		for (Cell[] array : cells) {
			for (Cell c : array) {
				if (c != null && c.getType() == CellType.AIR) {
					parts += ((AirCell)c).getNumberOfParticles();
				}
			}
		}
		return parts;
	}
	
	public List<Cell> getAlongVector(Vector2i start, Vector2f vec) {
		List<Vector2i> vecs = Utils.rasterizeLine(new Vector2f(start), new Vector2f(start).add(vec));
		List<Cell> results = new ArrayList<Cell>();
		for (Vector2i loc : vecs) {
			//Check if in bounds
			if ((loc.x > 0 && loc.x < width-1) && (loc.y > 0 && loc.y < height-1)) {
				results.add(cells[loc.x][loc.y]);
			}
		}
		//System.out.println("# of results: " + results.size());
		return results;
	}
	
	public float distanceTo(Vector2i start, Vector2i end) {
		float dX = start.x - end.x;
		float dY = start.y - end.y;
		return (float)Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
	}
	
	public Cell[][] getCells() {
		return cells;
	}
	
	public void attachCells(Game game) {
		for (Cell[] array : cells) {
			for (Cell c : array) {
				if (c != null) {
					game.attachChild(c);
					if(c.getType() == CellType.GROUND) {
					//System.out.println("Ground Cell! Size in world = " + c.getSizeInWorld());
					}
				}
			}
		}
	}
	
	private Vector2f getWorldPosition(int x, int y) {
		float worldX = ((x+0.5f)*cellSize)-(SCENE_SIZE/2);
		float worldY = ((y+0.5f)*cellSize)-(SCENE_SIZE/2);
		//System.out.println("Cell: (" + x + ", " + y +") world position = {" + worldX + ", " + worldY + "}"); 
		return new Vector2f(worldX, worldY);
	}
	
	
}
