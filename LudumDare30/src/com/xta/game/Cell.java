package com.xta.game;

import com.xta.entity.Entity;
import com.xta.game.cell.CellType;
import com.xta.math.Vector2f;
import com.xta.math.Vector2i;

public abstract class Cell extends Entity{
	
	public static final float GRAVITY = 1f;
	
	private final int gridX;
	private final int gridY;
	
	private final Grid grid;
	
	protected final Vector2i planetCenter;
	
	private float sizeinworld;
	
	public Cell(Grid grid, int gridX, int gridY, float sizeinworld, Vector2i planetCenter) {
		this.gridX = gridX;
		this.gridY = gridY;
		
		this.sizeinworld = sizeinworld;
		this.planetCenter = new Vector2i(planetCenter);
		
		this.grid = grid;
	}

	public Grid getGrid() {
		return grid;
	}
	
	public Vector2i getGridPosition() {
		return new Vector2i(gridX, gridY);
	}
	
	public int getGridX() {
		return gridX;
	}


	public int getGridY() {
		return gridY;
	}
	
	public float getSizeInWorld() {
		return sizeinworld;
	}
	
	public abstract CellType getType();
}
