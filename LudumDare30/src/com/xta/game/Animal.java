package com.xta.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import com.xta.RenderManager;
import com.xta.ResourceManager;
import com.xta.Utils;
import com.xta.entity.Entity;
import com.xta.math.GameMath;
import com.xta.math.Vector2f;

public class Animal extends Entity {

	private static final String[] BODIES = {"AnimalBody1.png", "AnimalBody2.png", 
		"AnimalBody3.png", "AnimalBody4.png", "AnimalBody5.png"};
	
	private static final String[] HEADS = {"AnimalHead1.png", "AnimalHead2.png", 
		"AnimalHead3.png"};
	
	private Image body;
	private Image head;
	
	public Animal() {
		body = ResourceManager.getInstance().getImage(BODIES[GameMath.randomInt(BODIES.length)]);
		head = ResourceManager.getInstance().getImage(HEADS[GameMath.randomInt(HEADS.length)]);
		Utils.swapColors((BufferedImage) body, Color.WHITE, Utils.generateRandomPastel());
		Utils.swapColors((BufferedImage) head, Color.WHITE, Utils.generateRandomPastel());
	}
	
	@Override
	protected void onUpdate(float deltaTime) {
	}

	@Override
	protected void onDraw(Graphics2D g2) {
		RenderManager.getInstance().drawImage(g2, body, new Vector2f(0, 0), 0.0f);
		RenderManager.getInstance().drawImage(g2, head, new Vector2f(-90, -50), 0.0f);
	}
}
