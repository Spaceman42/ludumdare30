package com.xta.game;

import java.awt.Graphics2D;
import java.util.SortedSet;
import java.util.TreeSet;

import com.xta.RenderManager;
import com.xta.ResourceManager;
import com.xta.entity.ReverseDepthComparator;
import com.xta.entity.Entity;
import com.xta.entity.Sprite;
import com.xta.graphics.Camera;
import com.xta.math.Vector2f;

public class Game {
	private ResourceManager res = ResourceManager.getInstance();
	private Camera camera;
	
	private SortedSet<Entity> children;
	private Grid grid;
	private double lastParts;
	public Game() {
		loadGraphcis();
		System.out.println("***Loading finished!***");
		
		camera = new Camera();
		RenderManager.getInstance().camera = camera;
		//Sets the camera center 
		//camera.setCenter(new Vector2(300, 300));
		
		
		
		children = new TreeSet<Entity>(new ReverseDepthComparator());
		
		//Animal a = new Animal();
		//attachChild(a);
		
		
		Planet planet = new Planet(350);
		
		grid = new Grid(1000, 1000, planet);
		grid.attachCells(this);
		lastParts = grid.getAllAirParts();
		attachChild(planet);
	}
	
	public void attachChild(Entity e) {
		children.add(e);
	}
	
	private void loadGraphcis() {
		res.loadImageDirectory("gfx/buildings");
		res.loadImageDirectory("gfx/animals");
	}
	
	public void onUpdate(float deltaTime) {
	//	camera.setCenter(camera.getLocation().add(new Vector2(1,0)));
		
		camera.setZoom(2f);
		double parts = grid.getAllAirParts();
		System.out.println(parts-lastParts);
		lastParts = parts;
		
		for (Entity e : children) {
			e.update(deltaTime);
		}
	}
	
	public void onDraw(Graphics2D g2) {
		
		for (Entity e : children) {
			e.draw(g2);
		}
	}
	
}
