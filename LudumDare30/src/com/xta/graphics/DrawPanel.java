package com.xta.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import com.xta.game.Game;

public class DrawPanel extends JPanel{
	
	public static final int WIDTH = 600;
	public static final int HEIGHT = 600;
	
	private Game game;
	
	
	public DrawPanel(Game game) {
		this.game = game;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		clearScreen(g2);
		
		game.onDraw(g2);
	}
	
	
	private void clearScreen(Graphics2D g2) {
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, getWidth(), getHeight());
	}
	
}
