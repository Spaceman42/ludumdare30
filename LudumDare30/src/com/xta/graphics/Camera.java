package com.xta.graphics;

import com.xta.math.Vector2f;

public class Camera {
	private Vector2f position;
	private float zoom;
	
	
	public Camera() {
		position = new Vector2f();
		zoom = 1.0f;
	}
	
	public void setCenter(Vector2f vec) {
		position = new Vector2f(vec);
	}
	
	public Vector2f getLocation() {
		return new Vector2f(position);
	}

	public float getZoom() {
		return zoom;
	}

	public void setZoom(float zoom) {
		//this.zoom -= 0.005f;
		if (zoom <= 1.0f) {
			zoom = 1.0f;
		}
	}
}
