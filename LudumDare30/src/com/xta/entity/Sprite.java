package com.xta.entity;

import java.awt.Graphics2D;
import java.awt.Image;

import com.xta.RenderManager;

public abstract class Sprite extends Entity {

	private Image image;
	
	public Sprite(Image image) {
		this.image = image;
	}
	
	public Sprite(Image image, float x, float y, float z) {
		this(image);
		setX(x);
		setY(y);
		setZ(z);
	}
	
	@Override
	protected abstract void onUpdate(float deltaTime);

	@Override
	protected void onDraw(Graphics2D g2) {
		RenderManager.getInstance().drawImage(g2, image, getPosition(), 0.0f);
	}
}
