package com.xta.entity;

import java.util.Comparator;

public class ReverseDepthComparator implements Comparator<Entity> {
	
	
	@Override
	public int compare(Entity e1, Entity e2) {
		float z1 = e1.getZ();
		float z2 = e2.getZ();
		
		float delta = z1 - z2;
		if (delta == 0) {
			delta = -1;
		}
		return -(int)delta;
	}
}
