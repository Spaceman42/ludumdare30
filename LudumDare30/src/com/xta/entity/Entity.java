package com.xta.entity;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;

import com.xta.math.Vector2f;

public abstract class Entity {
	private Collection<Entity> children;
	
	private Entity parent;
	
	private String id;
	
	private Vector2f position;
	
	//Depth
	private float z;
	
	public Entity() {
		position = new Vector2f();
		createChildCollection();
	}
	
	/**
	 * Override this method to change the way children are stored in the node. Defualt is ArrayList
	 */
	protected void createChildCollection() {
		children = new ArrayList<Entity>();
	}
	
	public void setID(String id) {
		this.id = id;
	}
	
	public String getID() {
		return id;
	}
	
	public Entity getParent() {
		return parent;
	}
	
	public void detachSelf() {
		parent.detachChild(this);
	}
	
	public void detachChild(Entity node) {
		children.remove(node);
	}
	
	public void attachChild(Entity node) {
		children.add(node);
	}
	
	public Collection<Entity> getChildren() {
		return children;
	}
	
	public void update(float deltaTime) {
		onUpdate(deltaTime);
		for (Entity e : children) {
			e.update(deltaTime);
		}
	}
	
	public void draw(Graphics2D g2) {
		onDraw(g2);
		for (Entity e : children) {
			e.draw(g2);
		}
	}
	/**
	 * Called once every game tick
	 * @param deltaTime time in milliseconds since last frame
	 */
	protected abstract void onUpdate(float deltaTime);
	
	/**
	 * Controls what the Entity draws
	 * @param g2 graphics object
	 */
	protected abstract void onDraw(Graphics2D g2);
	
	public Vector2f getPosition() {
		return new Vector2f(position);
	}
	
	public void setPosition(Vector2f vec) {
		position = new Vector2f(vec);
	}
	
	public float getX() {
		return position.x;
	}
	public void setX(float x) {
		position.x = x;
	}
	public float getY() {
		return position.y;
	}
	public void setY(float y) {
		position.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}
	
	
}
