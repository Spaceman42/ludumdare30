package com.xta;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.xta.math.GameMath;
import com.xta.math.Vector2f;
import com.xta.math.Vector2i;

public class Utils {
	private Utils() {}
	
	public static Image loadImage(String filePath) {
		return loadImage(new File(filePath));
	}
	
	
	public static Image loadImage(File file) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
	
	public static void swapColors(BufferedImage image, Color c1, Color c2) {
		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				if (image.getRGB(x, y) == c1.getRGB()) {
					image.setRGB(x, y, c2.getRGB());
				}
			}
		}
	}
	
	public static Color generateRandomColor(Color mix) {
	    int red = GameMath.randomInt(256);
	    int green = GameMath.randomInt(256);
	    int blue = GameMath.randomInt(256);
	    
	    // mix the color
	    if (mix != null) {
	        red = (red + mix.getRed()) / 2;
	        green = (green + mix.getGreen()) / 2;
	        blue = (blue + mix.getBlue()) / 2;
	    }

	    return new Color(red, green, blue);
	}
	
	public static Color generateRandomPastel() {
		return generateRandomColor(new Color(255, 255, 255));
	}
	
	public static List<Vector2i> vectorFloatToInt(List<Vector2f> list) {
		List<Vector2i> newList = new ArrayList<Vector2i>();
		for (Vector2f v : list) {
			newList.add(new Vector2i((int) v.x, (int) v.y));
		}
		return newList;
	}
	
	public static List<Vector2i> rasterizeLine(Vector2f start, Vector2f stop) {
		List<Vector2i> points = new ArrayList<Vector2i>();
		
		float x0 = start.x;
	    float y0 = start.y;
	    float x1 = stop.x;
	    float y1 = stop.y;
	    
	    //define vector differences and other variables required for Bresenham's Algorithm
	    float dx = Math.abs(x1 - x0);
	    float dy = Math.abs(y1 - y0);
	    
	    float sx = (x0 < x1) ? 1 : -1; //step x
	    float sy = (y0 < y1) ? 1 : -1; //step y
	    
	    float err = dx - dy; //get the initial error value
	    
	    //set the first point in the array
	    points.add(new Vector2i(x0, y0));
	    
	    //Main processing loop
	    
	    while(Math.abs(x1 - x0) >= 1.0f || Math.abs(y1 - y0) >= 1.0f) {
	        float e2 = err * 2; //hold the error value
	        
	        //use the error value to determine if the point should be rounded up or down
	        if(e2 >= -dy) {
	            err -= dy;
	            x0 += sx;
	        }
	        if(e2 < dx) {
	            err += dx;
	            y0 += sy;
	        }
	        
		    points.add(new Vector2i(x0, y0)); 
	    }		
		return points;
	}
}
