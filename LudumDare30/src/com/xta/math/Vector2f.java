package com.xta.math;

public class Vector2f {
	
	public float x;
	public float y;
	
	public Vector2f() {	
	}
	
	public Vector2f(Vector2i iVec) {
		this.x = (float)iVec.x;
		this.y = (float)iVec.y;
	}
	
	public Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2f(Vector2f v) {
		this.x = v.x;
		this.y = v.y;
	}
	
	public float length() {
		return (float) Math.sqrt(x * x + y * y);
	}
	
	public Vector2f normalize() {
		float length = length();
		x /= length;
		y /= length;
		return this;
	}
	
	public Vector2f add(Vector2f v) {
		x += v.x;
		y += v.y;
		return this;
	}
	
	public Vector2f sub(Vector2f v) {
		x -= v.x;
		y -= v.y;
		return this;
	}
	
	public Vector2f mul(Vector2f v) {
		x *= v.x;
		y *= v.y;
		return this;
	}
	
	public Vector2f div(Vector2f v) {
		x /= v.x;
		y /= v.y;
		return this;
	}
	
	public float dot(Vector2f v) {
		return x * v.x + y * v.y;
	}
	
	public float angleBetween(Vector2f v) {
		return (float) Math.acos(dot(v));
	}
	
	public float get(int i) {
		if (i == 0) {
			return x;
		} else if (i == 1) {
			return y;
		} else if (i == 2) {
			return 1.0f;
		} else {
			return Float.NaN;
		}
	}
	
	public void set(int i, float f) {
		if (i == 0) {
			x = f;
		} else if (i == 1) {
			y = f;
		}
	}
	
	public boolean equals(Object o) {
		if (o instanceof Vector2f) {
			Vector2f v = (Vector2f) o;
			return x == v.x && y == v.y;
		} else {
			return false;
		}
	}
	
	public String toString() {
		return "{ " + x + ", " + y + " }";
	}

	public Vector2f mul(float f) {
		x *= f;
		y *= f;
		return this;
	}
}
