package com.xta.math;

public class GameMath {
	
	public static final float PI = (float) Math.PI;
	public static final float TWO_PI = (float) Math.PI * 2.0f;
	
	public static int randomInt(int max) {
		return (int) (Math.random() * (float) max);
	}
	
	public static float sin(float f) {
		return (float) Math.sin(f);
	}
	
	public static float cos(float f) {
		return (float) Math.cos(f);
	}
}
