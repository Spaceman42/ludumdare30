package com.xta;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

import com.xta.graphics.Camera;
import com.xta.math.Vector2f;

public class RenderManager {
	private static final RenderManager INSTANCE = new RenderManager();
	
	public Camera camera;
	
	
	private RenderManager() {}
	
	
	public static RenderManager getInstance() {
		return INSTANCE;
	}
	
	private void startTranslate(Graphics2D g2) {
		AffineTransform trans = new AffineTransform();
		
		trans.translate(300, 300);
		trans.scale(1/camera.getZoom(), 1/camera.getZoom());
		trans.translate(-camera.getLocation().x, -camera.getLocation().y);
		
		g2.setTransform(trans);
	}

	public void drawLine(Graphics2D g2, Vector2f p0, Vector2f p1) {
		startTranslate(g2);
	}
	
	public void drawImage(Graphics2D g2, Image image, Vector2f position, float rotation) {
		startTranslate(g2);
		
		g2.translate(position.x - (image.getWidth(null)/2), position.y - (image.getHeight(null)/2));
		
		g2.drawImage(image, 0, 0, null);
	}
	
	public void drawRectangle(Graphics2D g2, Color color, Vector2f position, float width, float height) {
		startTranslate(g2);
		g2.translate(position.x - width/2, position.y - height/2);
		g2.setColor(color);
		g2.fillRect(0, 0, (int)width, (int)height);
	}
	
	public void drawCircle(Graphics2D g2, Vector2f center, float radius, Color color) {
		startTranslate(g2);
		g2.translate(center.x-radius, center.x-radius);
		g2.setColor(color);
		g2.fillOval(0, 0, (int)radius*2, (int)radius*2);
	}
}
